import { faker } from '@faker-js/faker'
import { Icons } from '../components'
import { WeatherResponseType } from '../utils'

export const cities = ['Teixeira de Freitas', 'Maringá', 'Salvador']
const conditions = ['Snow', 'Rain', 'Clear Day']

export const weatherMockResponse: WeatherResponseType = {
  days: Array.from({ length: 4 }, (_, index) => ({
    temp: faker.datatype.number(),
    conditions: faker.helpers.arrayElement(conditions),
    icon: faker.helpers.arrayElement(Icons),
    datetime: `2022-11-0${2 + index}T20:54:01.180Z`,
  })),
  current: {
    temp: faker.datatype.number(),
    conditions: faker.helpers.arrayElement(conditions),
    icon: faker.helpers.arrayElement(Icons),
    datetime: '2022-11-01T20:54:01.180Z',
  },
}
