import * as React from 'react'
import { createRoot } from 'react-dom/client'

import { App } from './app'

const cities = ['OTTAWA', 'MOSCOW', 'TOKYO']

// eslint-disable-next-line @typescript-eslint/no-non-null-assertion
const root = createRoot(document.getElementById('app')!)
root.render(<App cities={cities} />)
