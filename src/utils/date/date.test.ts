import { getDateRange, getDateWeek } from './date'

jest.useFakeTimers().setSystemTime(new Date('2022-11-05T05:17:38.963Z'))

describe('date utils', () => {
  test.each([
    [2, { start: '2022-11-05', end: '2022-11-07' }],
    [undefined, { start: '2022-11-05', end: '2022-11-09' }],
  ])('getDateRange return expected values', (range, expected) => {
    expect(getDateRange(range)).toEqual(expected)
  })

  test('getDateWeek return expected value', () => {
    expect(getDateWeek('2022-11-05T00:17:38-0300')).toBe('Sat')
  })
})
