export const userLocale =
  navigator.languages && navigator.languages.length
    ? navigator.languages[0]
    : navigator.language

export const getDateRange = (days = 4) => {
  const localeToFormat = 'en-CA'
  const now = new Date()
  const until = new Date()
  until.setDate(until.getDate() + days)

  return {
    start: now.toLocaleDateString(localeToFormat),
    end: until.toLocaleDateString(localeToFormat),
  }
}

export const getDateWeek = (dateString: string) => {
  const date = new Date(dateString)
  return date.toLocaleDateString(userLocale, {
    weekday: 'short',
  })
}
