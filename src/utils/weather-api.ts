import { IconType } from '../components'
import { getDateRange } from './date'

export type DayType = {
  datetime: string
  conditions: string
  temp: number
  icon: IconType
}

export type WeatherResponseType = {
  current: DayType
  days: DayType[]
}

export const weatherAPI = async (
  name: string
): Promise<WeatherResponseType> => {
  const dates = getDateRange()
  const data = await fetch(
    `https://weather.visualcrossing.com/VisualCrossingWebServices/rest/services/timeline/${name}/${dates.start}/${dates.end}?iconSet=icons2&unitGroup=metric&include=days&key=NTKFBJXYBJVXGGUUY7RUSU5DG&contentType=json`
  )
  const { days } = await data.json()

  return {
    current: days[0],
    days: days.splice(1),
  }
}
