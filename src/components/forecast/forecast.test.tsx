import * as React from 'react'
import { render, screen } from '@testing-library/react'
import { Forecast } from './forecast'
import { weatherMockResponse } from '../../test/fixtures'

describe('Forecast', () => {
  test('present the expected data', () => {
    const { days } = weatherMockResponse
    render(<Forecast days={days} />)

    const weekNames = ['Wed', 'Thu', 'Fri', 'Sat']

    screen.getAllByTestId('forecast-item').forEach((item, index) => {
      const day = days[index]
      expect(item.querySelector('.date')).toHaveTextContent(weekNames[index])
      expect(item.querySelector('.image-icon')).toHaveAttribute(
        'alt',
        day.conditions
      )
      expect(item.querySelector('.weather-value')).toHaveTextContent(
        `${day.temp}°`
      )
    })

    expect(screen.getAllByTestId('forecast-item')).toHaveLength(days.length)
  })
})
