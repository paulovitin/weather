import * as React from 'react'
import { DayType } from '../../utils'
import { ForecastItem } from './forecast-item'

export type ForecastProps = {
  days: DayType[]
}

export class Forecast extends React.Component<ForecastProps> {
  render() {
    const { days } = this.props
    return (
      <div className='forecast'>
        {days.map((item) => (
          <ForecastItem {...item} key={item.datetime} />
        ))}
      </div>
    )
  }
}
