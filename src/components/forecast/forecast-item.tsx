import * as React from 'react'
import { DayType, getDateWeek } from '../../utils'
import { Icon } from '../icon'

export type ForecastItemProps = DayType

export class ForecastItem extends React.Component<ForecastItemProps> {
  render() {
    const { datetime, icon, conditions, temp } = this.props
    return (
      <div key={datetime} className='days' data-testid='forecast-item'>
        <div className='date'>{getDateWeek(datetime)}</div>
        <div className='weather-icon'>
          <Icon name={icon} size={64} alt={conditions} />
        </div>
        <div className='weather-value'>{temp}°</div>
      </div>
    )
  }
}
