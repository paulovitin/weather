import * as React from 'react'
import { render, screen } from '@testing-library/react'
import { Headline } from './headline'
import { weatherMockResponse } from '../../test/fixtures'

describe('Headline', () => {
  test('present the correct data', () => {
    const [day] = weatherMockResponse.days
    render(<Headline {...day} />)

    const headline = screen.getByTestId('headline')

    expect(headline.querySelector('.date')).toHaveTextContent('Today')
    expect(headline.querySelector('.image-icon')).toHaveAttribute(
      'alt',
      day.conditions
    )
    expect(headline.querySelector('.weather-temperature')).toHaveTextContent(
      `${day.temp}°`
    )
    expect(headline.querySelector('.weather-description')).toHaveTextContent(
      day.conditions
    )
  })
})
