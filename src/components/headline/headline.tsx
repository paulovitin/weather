import * as React from 'react'
import { Icon, IconType } from '../icon'

export type HeadlineProps = {
  icon: IconType
  conditions: string
  temp: number
}

export class Headline extends React.Component<HeadlineProps> {
  render() {
    const { icon, conditions, temp } = this.props
    return (
      <div className='headline' data-testid='headline'>
        <div className='date'>Today</div>
        <div className='weather'>
          <div className='weather-icon'>
            <Icon size={120} name={icon} alt={conditions} />
          </div>
          <div className='weather-value'>
            <div className='weather-temperature'>{temp}°</div>
            <div className='weather-description'>{conditions}</div>
          </div>
        </div>
      </div>
    )
  }
}
