import * as React from 'react'
import imageUrls from '../../assets/**.svg'

export const Icons = [
  'snow',
  'snow-showers-day',
  'snow-showers-night',
  'thunder-rain',
  'thunder-showers-day',
  'thunder-showers-night',
  'rain',
  'showers-day',
  'showers-night',
  'fog',
  'wind',
  'cloudy',
  'partly-cloudy-day',
  'partly-cloudy-night',
  'clear-day',
  'clear-night',
] as const

export type IconType = typeof Icons[number]

export type IconProps = {
  size: number
  name: IconType
  alt: string
}

export class Icon extends React.Component<IconProps> {
  render() {
    const { name, size, alt } = this.props
    return (
      <img
        className='image-icon'
        src={imageUrls[name]}
        width={size}
        height={size}
        alt={alt}
      />
    )
  }
}
