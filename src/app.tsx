import * as React from 'react'

import { Cities, Weather } from './features'

export type AppProps = {
  cities: string[]
}

export type AppState = {
  city: string
}

export class App extends React.Component<AppProps, AppState> {
  constructor(props: AppProps) {
    super(props)

    const { cities } = this.props

    this.state = {
      city: cities[0],
    }
  }

  onSelectCity = (city: string) => {
    this.setState({ city })
  }

  render() {
    const { cities } = this.props
    const { city } = this.state

    return (
      <div className='wrapper'>
        <Cities
          cities={cities}
          selectedCity={city}
          onSelect={this.onSelectCity}
        />
        <Weather city={city} />
      </div>
    )
  }
}
