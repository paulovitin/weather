import * as React from 'react'
import { fireEvent, render, screen, waitFor } from '@testing-library/react'

import { weatherAPI } from './utils/weather-api'
import { weatherMockResponse, cities } from './test/fixtures'
import { App } from './app'

jest.mock('./utils/weather-api')

const weatherAPIMock = weatherAPI as jest.Mock<ReturnType<typeof weatherAPI>>

describe('App', () => {
  test('present the expected data', async () => {
    weatherAPIMock.mockReturnValue(Promise.resolve(weatherMockResponse))

    render(<App cities={cities} />)

    await waitFor(() => expect(screen.queryByText('Today')).toBeInTheDocument())

    const secondCityLink = screen.getByRole('link', {
      name: `See the ${cities[1]} weather`,
    })

    fireEvent.click(secondCityLink)

    await waitFor(() =>
      expect(screen.queryByTestId('loading')).toBeInTheDocument()
    )

    await waitFor(() => expect(screen.queryByText('Today')).toBeInTheDocument())

    expect(secondCityLink).toHaveClass('active')
  })
})
