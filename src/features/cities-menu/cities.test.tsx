import * as React from 'react'
import { fireEvent, render, screen } from '@testing-library/react'
import { Cities } from './cities'

describe('Cities Menu', () => {
  const mockOnSelect = (city: string) => {
    return city
  }
  const cities = ['Maringá', 'Salvador']

  test('present expected data', () => {
    render(
      <Cities
        cities={cities}
        selectedCity={cities[0]}
        onSelect={mockOnSelect}
      />
    )

    cities.forEach((city) => {
      expect(
        screen.getByRole('link', { description: `See the ${city} weather` })
      ).toBeInTheDocument()
    })

    expect(
      screen.getByRole('link', { name: `See the ${cities[0]} weather` })
    ).toHaveClass('active')
  })

  test('on click called onSelect function', () => {
    const onSelectMock = jest.fn()
    render(
      <Cities
        cities={cities}
        selectedCity={cities[0]}
        onSelect={onSelectMock}
      />
    )

    const link = screen.getByRole('link', {
      name: `See the ${cities[1]} weather`,
    })

    fireEvent.click(link)

    expect(onSelectMock).toHaveBeenCalledWith(cities[1])
  })
})
