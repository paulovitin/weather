import * as React from 'react'
import classnames from 'classnames'

type CitiesType = {
  onSelect: (city: string) => void
  cities: string[]
  selectedCity: string
}

export class Cities extends React.Component<CitiesType> {
  onClick = (city: string) => (evt: React.MouseEvent<HTMLElement>) => {
    evt.preventDefault()
    this.props.onSelect(city)
  }

  render() {
    const { cities, selectedCity } = this.props
    return (
      <div className='cities'>
        <nav>
          {cities.map((city) => (
            <a
              key={city}
              onClick={this.onClick(city)}
              className={classnames({
                active: city === selectedCity,
              })}
              title={`See the ${city} weather`}
              href='#'
            >
              {city}
            </a>
          ))}
        </nav>
      </div>
    )
  }
}
