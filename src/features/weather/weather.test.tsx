import * as React from 'react'
import { render, screen, waitFor } from '@testing-library/react'
import { Weather } from './weather'
import { weatherAPI } from '../../utils/weather-api'
import { weatherMockResponse } from '../../test/fixtures'

jest.mock('../../utils/weather-api')

const weatherAPIMock = weatherAPI as jest.Mock<ReturnType<typeof weatherAPI>>

jest.useFakeTimers()

describe('Weather', () => {
  test('present expected data', async () => {
    weatherAPIMock.mockReturnValue(Promise.resolve(weatherMockResponse))

    render(<Weather city='Maringá' />)

    // loading
    expect(screen.getByTestId('loading')).toBeInTheDocument()

    // await loaded
    await waitFor(() => expect(screen.queryByText('Today')).toBeInTheDocument())

    expect(screen.getByTestId('headline')).toBeInTheDocument()
    expect(screen.getAllByTestId('forecast-item')).toHaveLength(
      weatherMockResponse.days.length
    )
  })

  test('auto update data by time', async () => {
    weatherAPIMock.mockReturnValue(Promise.resolve(weatherMockResponse))

    render(<Weather city='Maringá' />)

    // loading
    await waitFor(() => expect(screen.queryByText('Today')).toBeInTheDocument())

    // check first response temperature
    expect(
      screen.queryByText(`${weatherMockResponse.current.temp}°`)
    ).toBeInTheDocument()

    // run the timers
    jest.runAllTimers()

    // change the response temperature
    const weatherMockUpdateResponse = {
      ...weatherMockResponse,
      current: {
        ...weatherMockResponse.current,
        temp: 11,
      },
    }

    weatherAPIMock.mockReturnValue(Promise.resolve(weatherMockUpdateResponse))

    // check the update had success
    await waitFor(() =>
      expect(
        screen.queryByText(`${weatherMockUpdateResponse.current.temp}°`)
      ).toBeInTheDocument()
    )
  })
})
