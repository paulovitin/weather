import * as React from 'react'
import ContentLoader from 'react-content-loader'

export const Loading = () => (
  <ContentLoader
    data-testid='loading'
    speed={2}
    width={636}
    height={485}
    viewBox='0 0 636 485'
    backgroundColor='#f3f3f3'
    foregroundColor='#ecebeb'
  >
    <path d='M 273 37 h 94 v 28 h -94 z M 298 91 h 94 v 64 h -94 z M 149 91 h 120 v 103 H 149 z M 0 292 h 156 v 193 H 0 z M 160 292 h 156 v 193 H 160 z M 320 292 h 156 v 193 H 320 z M 480 292 h 156 v 193 H 480 z M 298 159 h 203 v 35 H 298 z' />
  </ContentLoader>
)
