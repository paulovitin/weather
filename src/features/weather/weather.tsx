import * as React from 'react'

import { weatherAPI, WeatherResponseType } from '../../utils'
import { Forecast } from '../../components'
import { Loading } from './loading'
import { Headline } from '../../components/headline'

export type WeatherProps = {
  city: string
}

export type WeatherState = {
  fetching: boolean
  data?: WeatherResponseType
}

const refreshMinutesInMS = 1 * 60 * 1000 // 5 minutes

export class Weather extends React.Component<WeatherProps, WeatherState> {
  timeout?: ReturnType<typeof setTimeout>

  constructor(props: WeatherProps) {
    super(props)
    this.state = {
      fetching: true,
    }
  }

  async fetch() {
    const data = await weatherAPI(this.props.city)
    this.setState({ data, fetching: false })

    if (this.timeout) clearTimeout(this.timeout)

    await new Promise((resolve) => {
      this.timeout = setTimeout(resolve, refreshMinutesInMS)
    })

    this.fetch()
  }

  async componentDidMount() {
    this.fetch()
  }

  async componentDidUpdate(prevProps: Readonly<WeatherProps>) {
    if (prevProps.city !== this.props.city) {
      this.setState({ fetching: true })
      this.fetch()
    }
  }

  componentWillUnmount() {
    if (this.timeout) clearTimeout(this.timeout)
  }

  render() {
    if (!this.state.data || this.state.fetching) {
      return (
        <main>
          <Loading />
        </main>
      )
    }

    const { data } = this.state
    return (
      <main>
        <Headline {...data.current} />
        <Forecast days={data.days} />
      </main>
    )
  }
}
