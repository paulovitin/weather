<h1 align="center">Welcome to AgencyAnalytics Weather 👋</h1>
<p>
  <img alt="Version" src="https://img.shields.io/badge/version-1.0.0-blue.svg?cacheSeconds=2592000" />
  <a href="#" target="_blank">
    <img alt="License: MIT" src="https://img.shields.io/badge/License-MIT-yellow.svg" />
  </a>
  <a href="https://twitter.com/paulovitin" target="_blank">
    <img alt="Twitter: paulovitin" src="https://img.shields.io/twitter/follow/paulovitin.svg?style=social" />
  </a>
</p>

> Simple app to display the weather in three cities. (AgencyAnalytics code challenge)

### ✨ [Demo](https://weather-paulovitin.vercel.app/)

## Install

```sh
yarn install
```

## Usage

```sh
yarn run start
```

## Usage

```sh
yarn run build
```

## Run tests

```sh
yarn run test
```

## Run lint

```sh
yarn run lint
```

With fix

```sh
yarn run lint --fix
```

## Run Typescript Check

```sh
yarn run check-types
```

## Author

👤 **Paulo Reis<paulovitin@gmail.com>**

- Twitter: [@paulovitin](https://twitter.com/paulovitin)
- LinkedIn: [@https:\/\/www.linkedin.com\/in\/paulo-vitor-reis-a38687165\/](https://linkedin.com/in/https://www.linkedin.com/in/paulo-vitor-reis-a38687165/)

---

_This README was generated with ❤️ by [readme-md-generator](https://github.com/kefranabg/readme-md-generator)_
